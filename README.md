# TODO-APP

## Settings

* From your terminal, run the following command:
```
docker run -d \
  --name my-mariadb \
  -e MARIADB_ROOT_USERNAME=root \
  -e MARIADB_ROOT_PASSWORD=rootpwd \
  -p 3306:3306 \
  -v db-mariadb:/var/lib/mariadb \
  mariadb:latest
```

(You can remove the `-d` flag if you prefer to run the instance from your terminal).

* Create a database and name it `todo`.

* Run `gradle clean build`.

* Run `gradle clean update`. This will trigger the Liquibase plugin and automatically setup the database for you.

* You can now run the application.