package ch.cern.todo.helper;

public class DateHelper {
    private DateHelper () {
        throw new IllegalArgumentException("Utility classes should not be instantiated.");
    }

    public static final String EU_DATETIME_FORMAT = "dd-MM-yyyy HH:mm";
}
