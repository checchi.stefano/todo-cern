package ch.cern.todo.mapper;

import ch.cern.todo.dto.task.SaveTaskRequestDTO;
import ch.cern.todo.dto.task.TaskDTO;
import ch.cern.todo.dto.task.UpdateTaskRequestDTO;
import ch.cern.todo.entity.Task;
import ch.cern.todo.entity.TaskCategory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.data.domain.Page;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface TaskMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "category", ignore = true)
    Task toEntity(TaskDTO dto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "category", source = "category")
    @Mapping(target = "description", source = "requestDTO.description")
    @Mapping(target = "name", source = "requestDTO.name")
    Task toEntity(SaveTaskRequestDTO requestDTO, TaskCategory category);

    @Mapping(target = "categoryId", source = "entity.category.id")
    TaskDTO toDTO(Task entity);

    default Page<TaskDTO> toDTO(Page<Task> tasks) {
        return tasks.map(this::toDTO);
    }

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "category", ignore = true)
    @Mapping(target = "description", source = "requestDTO.description")
    @Mapping(target = "name", source = "requestDTO.name")
    void updateFromDTO(UpdateTaskRequestDTO requestDTO, @MappingTarget Task entity);
}
