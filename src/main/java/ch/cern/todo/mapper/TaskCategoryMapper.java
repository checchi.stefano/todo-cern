package ch.cern.todo.mapper;

import ch.cern.todo.dto.category.SaveTaskCategoryRequestDTO;
import ch.cern.todo.dto.category.TaskCategoryDTO;
import ch.cern.todo.dto.category.UpdateTaskCategoryRequestDTO;
import ch.cern.todo.entity.TaskCategory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.data.domain.Page;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface TaskCategoryMapper {
    @Mapping(target = "id", ignore = true)
    TaskCategory toEntity(TaskCategoryDTO dto);

    @Mapping(target = "id", ignore = true)
    TaskCategory toEntity(SaveTaskCategoryRequestDTO dto);

    TaskCategoryDTO toDTO(TaskCategory entity);

    default Page<TaskCategoryDTO> toDTO(Page<TaskCategory> categories) {
        return categories.map(this::toDTO);
    }

    @Mapping(target = "id", ignore = true)
    void updateFromDTO(UpdateTaskCategoryRequestDTO dto, @MappingTarget TaskCategory entity);
}
