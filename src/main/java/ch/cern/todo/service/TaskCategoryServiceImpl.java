package ch.cern.todo.service;

import ch.cern.todo.dto.category.SaveTaskCategoryRequestDTO;
import ch.cern.todo.dto.category.TaskCategoryDTO;
import ch.cern.todo.dto.category.UpdateTaskCategoryRequestDTO;
import ch.cern.todo.entity.TaskCategory;
import ch.cern.todo.mapper.TaskCategoryMapper;
import ch.cern.todo.repository.TaskCategoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import static ch.cern.todo.repository.CustomRepository.NOT_FOUND_ENTITY;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskCategoryServiceImpl implements TaskCategoryService {

    private final TaskCategoryMapper taskCategoryMapper;
    private final TaskCategoryRepository taskCategoryRepository;

    /**
     * Creates a new Task Category.
     * @param request the body of the request.
     * @return the created Task Category.
     */
    @Override
    @Transactional
    public TaskCategoryDTO createTaskCategory(SaveTaskCategoryRequestDTO request) {
        TaskCategory category = taskCategoryRepository.save(taskCategoryMapper.toEntity(request));

        return taskCategoryMapper.toDTO(category);
    }

    /**
     * Gets a Task Category by ID.
     * @param id the ID of the target Task Category.
     * @return the requested Task Category.
     */
    @Override
    public TaskCategoryDTO getTaskCategoryById(Integer id) {
        TaskCategory category = taskCategoryRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format(NOT_FOUND_ENTITY, "Category", "ID", id)));

        return taskCategoryMapper.toDTO(category);
    }

    /**
     * Updates a Task Category. <br/>
     * The target Task Category can be retrieved using either its ID or name.
     * @param updateRequestDTO the update request.
     * @return the updated Task Category.
     */
    @Override
    @Transactional
    public TaskCategoryDTO updateTaskCategory(UpdateTaskCategoryRequestDTO updateRequestDTO) {
        if (updateRequestDTO == null || (updateRequestDTO.getId() == null && updateRequestDTO.getOldName() == null))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: Cannot retrieve the Task Category without its ID or name");

        Integer id = updateRequestDTO.getId();
        String oldName = updateRequestDTO.getOldName();

        TaskCategory category = taskCategoryRepository.findByIdOrName(id, oldName, "Category");

        taskCategoryMapper.updateFromDTO(updateRequestDTO, category);

        return taskCategoryMapper.toDTO(category);
    }

    /**
     * Deletes a Task Category by ID.
     * @param id the ID of the target Task Category.
     */
    @Override
    public void deleteTaskCategory(Integer id) {
        if (taskCategoryRepository.existsById(id)) {
            taskCategoryRepository.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format(NOT_FOUND_ENTITY, "Category", "ID", id));
        }
    }

    /**
     * Returns a {@link Page} of Task Categories.
     * @param pageable the {@link Pageable} object in the request.
     * @return a {@link Page} of Task Categories.
     */
    @Override
    public Page<TaskCategoryDTO> getTaskCategories(Pageable pageable) {
        Page<TaskCategory> categories = taskCategoryRepository.findAll(pageable);
        return taskCategoryMapper.toDTO(categories);
    }
}
