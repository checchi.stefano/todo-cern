package ch.cern.todo.service;

import ch.cern.todo.dto.category.SaveTaskCategoryRequestDTO;
import ch.cern.todo.dto.task.SaveTaskRequestDTO;
import ch.cern.todo.dto.task.TaskDTO;
import ch.cern.todo.dto.task.UpdateTaskRequestDTO;
import ch.cern.todo.entity.Task;
import ch.cern.todo.entity.TaskCategory;
import ch.cern.todo.mapper.TaskMapper;
import ch.cern.todo.repository.TaskCategoryRepository;
import ch.cern.todo.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static ch.cern.todo.repository.CustomRepository.NOT_FOUND_ENTITY;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final TaskCategoryRepository taskCategoryRepository;
    private final TaskMapper taskMapper;

    /**
     * Creates a new Task.<br/>
     * The {@link TaskCategory} to associate can be retrieved either by ID (through {@code categoryId}) or by name
     * (through {@code categoryRequest.name}, if existing), or it can even be created on the fly
     * (through {@code categoryRequest}).
     *
     * @param request the body of the request.
     * @return the created Task.
     */
    @Override
    @Transactional
    public TaskDTO createTask(SaveTaskRequestDTO request) {
        TaskCategory category = null;

        if (request.getCategoryId() == null && (request.getCategoryRequest() == null || request.getCategoryRequest().getName() == null)) {
            log.warn("Not enough information was provided to associate the task with a category. Will attempt to create the task without a category...");
        } else if (request.getCategoryId() != null && taskCategoryRepository.existsById(request.getCategoryId())) {
            category = taskCategoryRepository.findById(request.getCategoryId()).get();
        } else {
            category = getOrCreateCategory(request.getCategoryRequest());
        }

        Task task = taskRepository.save(taskMapper.toEntity(request, category));

        return taskMapper.toDTO(task);
    }

    private TaskCategory getOrCreateCategory(SaveTaskCategoryRequestDTO request) {
        if (request == null || request.getName() == null)
            return null;

        Optional<TaskCategory> categoryOpt = taskCategoryRepository.findByName(request.getName());
        if (categoryOpt.isPresent())
            return categoryOpt.get();

        var newCategory = new TaskCategory();
        newCategory.setName(request.getName());
        newCategory.setDescription(request.getDescription());

        return taskCategoryRepository.save(newCategory);
    }

    /**
     * Gets a Task by ID.
     *
     * @param id the ID of the target Task.
     * @return the requested Task.
     */
    @Override
    public TaskDTO getTaskById(Integer id) {
        Task task = taskRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format(NOT_FOUND_ENTITY, "Task", "ID", id)));

        return taskMapper.toDTO(task);
    }

    /**
     * Updates a Task.<br/>
     * The target Task can be retrieved using either its ID or name.
     *
     * @param updateRequestDTO the update request.
     * @return the updated Task.
     */
    @Override
    @Transactional
    public TaskDTO updateTask(UpdateTaskRequestDTO updateRequestDTO) {
        if (updateRequestDTO == null || (updateRequestDTO.getId() == null && updateRequestDTO.getOldName() == null))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: Cannot retrieve the Task  without its ID or name");

        Integer id = updateRequestDTO.getId();
        String oldName = updateRequestDTO.getOldName();
        Task task = taskRepository.findByIdOrName(id, oldName, "Task");
        setCategoryFromRequest(updateRequestDTO, task);

        taskMapper.updateFromDTO(updateRequestDTO, task);

        return taskMapper.toDTO(task);
    }

    private void setCategoryFromRequest(UpdateTaskRequestDTO updateRequestDTO, Task task) {
        Optional<TaskCategory> categoryOpt = Optional.empty();
        if (updateRequestDTO.getCategoryId() != null)
            categoryOpt = taskCategoryRepository.findById(updateRequestDTO.getCategoryId());
        categoryOpt.ifPresent(task::setCategory);
    }

    /**
     * Deletes a Task by ID.
     *
     * @param id the ID of the target Task.
     */
    @Override
    public void deleteTask(Integer id) {
        if (taskRepository.existsById(id)) {
            taskRepository.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format(NOT_FOUND_ENTITY, "Task", "ID", id));
        }
    }

    /**
     * Returns a {@link Page} of Tasks.
     *
     * @param pageable the {@link Pageable} object in the request.
     * @return a {@link Page} of Tasks.
     */
    @Override
    public Page<TaskDTO> getTasks(Pageable pageable) {
        Page<Task> tasks = taskRepository.findAll(pageable);

        return taskMapper.toDTO(tasks);
    }
}
