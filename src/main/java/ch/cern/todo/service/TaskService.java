package ch.cern.todo.service;

import ch.cern.todo.dto.task.SaveTaskRequestDTO;
import ch.cern.todo.dto.task.TaskDTO;
import ch.cern.todo.dto.task.UpdateTaskRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TaskService {
    TaskDTO createTask(SaveTaskRequestDTO request);

    TaskDTO getTaskById(Integer id);

    TaskDTO updateTask(UpdateTaskRequestDTO updateRequest);

    void deleteTask(Integer id);

    Page<TaskDTO> getTasks(Pageable pageable);
}
