package ch.cern.todo.service;

import ch.cern.todo.dto.category.SaveTaskCategoryRequestDTO;
import ch.cern.todo.dto.category.TaskCategoryDTO;
import ch.cern.todo.dto.category.UpdateTaskCategoryRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TaskCategoryService {
    TaskCategoryDTO createTaskCategory(SaveTaskCategoryRequestDTO request);

    TaskCategoryDTO getTaskCategoryById(Integer id);

    TaskCategoryDTO updateTaskCategory(UpdateTaskCategoryRequestDTO updateRequest);

    void deleteTaskCategory(Integer id);

    Page<TaskCategoryDTO> getTaskCategories(Pageable pageable);
}
