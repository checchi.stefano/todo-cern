package ch.cern.todo.repository;

import ch.cern.todo.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaskRepository extends CustomRepository<Task, Integer> {
    Optional<Task> findByName(String name);
}
