package ch.cern.todo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@NoRepositoryBean
public interface CustomRepository<T, ID> extends JpaRepository<T, ID> {
    String NOT_FOUND_ENTITY = "%1s with %2s = %3s could not be found.";

    /**
     * Retrieves an entity either by its ID or by its name.
     * @param id the ID of the target entity.
     * @param name the name of the target entity.
     * @return the requested entity.
     */
    default T findByIdOrName(ID id, String name, String entityName) {
        if (id != null && this.existsById(id)) {
            return this.findById(id).get();
        } else if (name != null) {
            return this.findByName(name).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format(NOT_FOUND_ENTITY, entityName, "name", name))
            );
        }

        throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format(NOT_FOUND_ENTITY, entityName, "ID", id));
    }

    Optional<T> findByName(String name);
}
