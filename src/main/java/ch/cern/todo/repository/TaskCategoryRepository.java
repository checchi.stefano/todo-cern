package ch.cern.todo.repository;

import ch.cern.todo.entity.TaskCategory;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaskCategoryRepository extends CustomRepository<TaskCategory, Integer> {
    Optional<TaskCategory> findByName(String name);
}