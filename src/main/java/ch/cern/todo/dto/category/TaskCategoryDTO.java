package ch.cern.todo.dto.category;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskCategoryDTO {
    @Schema(description = "Category ID", example = "1")
    private Integer id;

    @NotBlank
    @Schema(description = "Category name", example = "Unit test")
    private String name;

    @Schema(description = "Category description", example = "Category to group UT related tasks")
    private String description;
}
