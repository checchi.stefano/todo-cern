package ch.cern.todo.dto.category;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateTaskCategoryRequestDTO {
    @Schema(description = "Category ID", example = "Unit test")
    private Integer id;

    @Schema(description = "Category's old name", example = "Unit test")
    private String oldName;

    @Schema(description = "Category's new name", example = "Unit test")
    private String name;

    @Schema(description = "Category description", example = "Category to group UT related tasks")
    private String description;
}
