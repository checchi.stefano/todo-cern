package ch.cern.todo.dto.task;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import static ch.cern.todo.helper.DateHelper.EU_DATETIME_FORMAT;

@Getter
@Setter
public class TaskDTO {
    @Schema(description = "Task ID", example = "1")
    private String id;

    @NotBlank
    @Schema(description = "Task name", example = "70% Sonar coverage")
    private String name;

    @Schema(description = "Category description", example = "Add unit tests to achieve expected coverage")
    private String description;

    @Schema(description = "Task deadline", example = "01-11-2024 08:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = EU_DATETIME_FORMAT)
    private LocalDateTime deadline;

    @Schema(description = "The ID of the associated category", example = "1")
    private Integer categoryId;
}
