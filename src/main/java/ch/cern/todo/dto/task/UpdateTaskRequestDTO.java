package ch.cern.todo.dto.task;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import static ch.cern.todo.helper.DateHelper.EU_DATETIME_FORMAT;

@Getter
@Setter
public class UpdateTaskRequestDTO {
    @Schema(description = "Task ID", example = "1")
    private Integer id;

    @Schema(description = "Task's old name", example = "Unit test")
    private String oldName;

    @Schema(description = "Task's new name", example = "Unit test")
    private String name;

    @Schema(description = "Task description", example = "Add unit tests to achieve expected coverage")
    private String description;

    @Schema(description = "Task deadline", example = "01-11-2024 08:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = EU_DATETIME_FORMAT)
    private LocalDateTime deadline;

    @Schema(description = "The ID of the associated category", example = "1")
    private Integer categoryId;
}
