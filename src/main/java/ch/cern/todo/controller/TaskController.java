package ch.cern.todo.controller;

import ch.cern.todo.dto.category.TaskCategoryDTO;
import ch.cern.todo.dto.task.SaveTaskRequestDTO;
import ch.cern.todo.dto.task.TaskDTO;
import ch.cern.todo.dto.task.UpdateTaskRequestDTO;
import ch.cern.todo.service.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("tasks")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;

    @Operation(summary = "Create a task")
    @PostMapping
    public ResponseEntity<TaskDTO> createTask(@Valid @RequestBody @NotNull final SaveTaskRequestDTO request) {
        TaskDTO operationIntervention = taskService.createTask(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(operationIntervention);
    }

    @Operation(summary = "Get a page of tasks")
    @GetMapping("/search")
    public ResponseEntity<Page<TaskDTO>> getTasks(@PageableDefault(sort = {"name"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return ResponseEntity.ok(taskService.getTasks(pageable));
    }

    @Operation(summary = "Get task by ID")
    @GetMapping("/{task_id}")
    public ResponseEntity<TaskDTO> getTaskById(@Parameter(example = "1") @PathVariable("task_id") Integer id) {
        return ResponseEntity.ok(taskService.getTaskById(id));
    }

    @Operation(summary = "Update task")
    @PutMapping
    public ResponseEntity<TaskDTO> updateTask(@Valid @RequestBody @NotNull UpdateTaskRequestDTO updateRequest) {
        return ResponseEntity.ok(taskService.updateTask(updateRequest));
    }

    @Operation(summary = "Delete task by ID")
    @DeleteMapping("/{task_id}")
    public ResponseEntity<Void> deleteTask(@Parameter(example = "1") @PathVariable("task_id") Integer id) {
        taskService.deleteTask(id);
        return ResponseEntity.noContent().build();
    }

}
