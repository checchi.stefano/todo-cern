package ch.cern.todo.controller;

import ch.cern.todo.dto.category.SaveTaskCategoryRequestDTO;
import ch.cern.todo.dto.category.TaskCategoryDTO;
import ch.cern.todo.dto.category.UpdateTaskCategoryRequestDTO;
import ch.cern.todo.dto.task.SaveTaskRequestDTO;
import ch.cern.todo.dto.task.TaskDTO;
import ch.cern.todo.dto.task.UpdateTaskRequestDTO;
import ch.cern.todo.service.TaskCategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("categories")
@RequiredArgsConstructor
public class TaskCategoryController {

    private final TaskCategoryService taskCategoryService;

    @Operation(summary = "Create a task category")
    @PostMapping
    public ResponseEntity<TaskCategoryDTO> createTask(@Valid @RequestBody final SaveTaskCategoryRequestDTO request) {
        TaskCategoryDTO category = taskCategoryService.createTaskCategory(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(category);
    }

    @Operation(summary = "Get a page of task categories")
    @GetMapping("/search")
    public ResponseEntity<Page<TaskCategoryDTO>> getTaskCategories(@PageableDefault(sort = {"name"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return ResponseEntity.ok(taskCategoryService.getTaskCategories(pageable));
    }

    @Operation(summary = "Get a task category by ID")
    @GetMapping("/{task_category_id}")
    public ResponseEntity<TaskCategoryDTO> getTaskById(@Parameter(example = "1") @PathVariable("task_category_id") Integer id) {
        return ResponseEntity.ok(taskCategoryService.getTaskCategoryById(id));
    }

    @Operation(summary = "Update a task category")
    @PutMapping
    public ResponseEntity<TaskCategoryDTO> updateTaskCategory(@Valid @RequestBody @NotNull UpdateTaskCategoryRequestDTO updateRequest) {
        return ResponseEntity.ok(taskCategoryService.updateTaskCategory(updateRequest));
    }

    @Operation(summary = "Delete a task category by ID")
    @DeleteMapping("/{task_category_id}")
    public ResponseEntity<Void> deleteTaskCategory(@Parameter(example = "1") @PathVariable("task_category_id") Integer id) {
        taskCategoryService.deleteTaskCategory(id);
        return ResponseEntity.noContent().build();
    }

}